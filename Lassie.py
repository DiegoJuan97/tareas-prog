#POO Clases
class Perro:
    especie='mamifero'
    def __init__(self,nombre,edad):
        self.nombre = nombre
        self.edad = edad    
    
    def descripcion(self):
        return"{} es {} años de edad".format(self.nombre,self.edad)

    def hablar (self, sonido):
        return"{} dice {}".format(self.nombre,sonido)

class Rottweiler(Perro):
     def correr(self,velocidad):
       print ("{} corre a {}".format(self.nombre, velocidad))

lassie = Perro("Lassie",5)
print(lassie.descripcion())
print(lassie.hablar("Woof Woof"))
roco = Rottweiler("Roco",10)
roco.correr(50)
print(roco.hablar("Que eh lo que eh"))